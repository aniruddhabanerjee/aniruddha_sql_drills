create table patient(
    id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    DOB DATE NOT NULL,
    Address VARCHAR(255) NOT NULL
);

create table doctor(
    id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL
);

create table secretary(
    id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL
);

create table prescription(
    Prescription_id INT AUTO_INCREMENT PRIMARY KEY,
    Patient_id INT,
    Drug VARCHAR(100) NOT NULL,
    Todays_date Date NOT NULL,
    Dosage INT NOT NULL,
    Doctor_id INT,
    Secretary_id INT,
    FOREIGN KEY(Patient_id) REFERENCES patient(id),
    FOREIGN KEY(Doctor_id) REFERENCES doctor(id),
    FOREIGN KEY(Secretary_id) REFERENCES secretary(id)
);