create table manager(
    id INT PRIMARY KEY,
    Manager_name VARCHAR(100) NOT NULL,
    Manager_location VARCHAR(100) NOT NULL
);

create table contract(
    id INT PRIMARY KEY,
    Estimated_cost FLOAT NOT NULL,
    Completion_date DATE NOT NULL
);

create table staff(
    id INT PRIMARY KEY,
    staff_name VARCHAR(100) NOT NULL,
    staff_location varchar(255) NOT NULL
);

create table client(
    client_id INT PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    location VARCHAR(255) NOT NULL,
    manager_id INT,
    contract_id INT,
    staff_id INT,
    FOREIGN KEY(manager_id) REFERENCES manager(id),
    FOREIGN KEY(contract_id) REFERENCES contract(id),
    FOREIGN KEY(staff_id) REFERENCES staff(id)
);
