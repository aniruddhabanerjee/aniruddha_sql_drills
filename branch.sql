create table branch(
    id INT AUTO_INCREMENT PRIMARY KEY,
    Branch_addr VARCHAR(255) NOT NULL
);

create table author(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

create table publisher(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL 
);

create table books(
    id int AUTO_INCREMENT PRIMARY KEY,
    ISBN VARCHAR(50) NOT NULL,
    Title VARCHAR(255) NOT NULL,
    Branch_id INT,
    Author_id INT,
    Publisher_id INT,
    Num_copies INT NOT NULL,
    FOREIGN KEY(Branch_id) REFERENCES branch(id),
    FOREIGN KEY(Author_id) REFERENCES author(id),
    FOREIGN KEY(Publisher_id) REFERENCES publisher(id)
);

