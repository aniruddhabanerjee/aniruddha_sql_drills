create table doctor(
    id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Secretary VARCHAR(100) NOT NULL
);

create table patient(
    id INT AUTO_INCREMENT PRIMARY KEY,
    PatientName VARCHAR(100) NOT NULL,
    PatientDOB DATE NOT NULL,
    PatientAddress VARCHAR(255) NOT NULL
);

create table prescription(
    Prescription_id INT AUTO_INCREMENT PRIMARY KEY,
    Patient_id INT,
    Doctor_id INT,
    Drug VARCHAR(100) NOT NULL,
    Todays_date DATE NOT NULL,
    Dosage INT NOT NULL,
    FOREIGN KEY(Patient_id) REFERENCES patient(id),
    FOREIGN KEY(Doctor_id) REFERENCES doctor(id)
);